FROM maven:latest AS build
COPY src /home/app/src
COPY pom.xml /home/app
WORKDIR /home/app
RUN mvn clean package -DskipTests

FROM eclipse-temurin:17-jdk-alpine
WORKDIR /app
COPY --from=build /home/app/target/articles-microservice-latest.jar .
CMD ["java", "-jar", "articles-microservice-latest.jar"]