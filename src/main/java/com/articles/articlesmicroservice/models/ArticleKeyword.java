package com.articles.articlesmicroservice.models;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class ArticleKeyword {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
}