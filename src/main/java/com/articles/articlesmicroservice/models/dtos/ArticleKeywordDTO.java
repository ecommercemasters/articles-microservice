package com.articles.articlesmicroservice.models.dtos;

import lombok.Data;

@Data
public class ArticleKeywordDTO {
    private String name;
}
