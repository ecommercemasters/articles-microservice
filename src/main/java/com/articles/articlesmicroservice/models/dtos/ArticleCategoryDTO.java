package com.articles.articlesmicroservice.models.dtos;

import lombok.Data;

@Data
public class ArticleCategoryDTO {
    private String name;
}
