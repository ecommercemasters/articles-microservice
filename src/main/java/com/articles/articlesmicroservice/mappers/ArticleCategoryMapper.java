package com.articles.articlesmicroservice.mappers;

import com.articles.articlesmicroservice.models.ArticleCategory;
import com.articles.articlesmicroservice.models.dtos.ArticleCategoryDTO;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

public class ArticleCategoryMapper {
    private static final ModelMapper modelMapper = new ModelMapper();

    public static ArticleCategoryDTO toDTO(ArticleCategory articleCategory) {
        return modelMapper.map(articleCategory, ArticleCategoryDTO.class);
    }

    public static ArticleCategory toEntity(ArticleCategoryDTO articleCategoryDTO) {
        return modelMapper.map(articleCategoryDTO, ArticleCategory.class);
    }

    public static List<ArticleCategoryDTO> toDTOList(List<ArticleCategory> articleCategories) {
        return articleCategories.stream()
                .map(ArticleCategoryMapper::toDTO)
                .collect(Collectors.toList());
    }

    public static List<ArticleCategory> toEntityList(List<ArticleCategoryDTO> articleCategoryDTOs) {
        return articleCategoryDTOs.stream()
                .map(ArticleCategoryMapper::toEntity)
                .collect(Collectors.toList());
    }
}
