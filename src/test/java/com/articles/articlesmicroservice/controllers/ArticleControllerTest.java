package com.articles.articlesmicroservice.controllers;

import com.articles.articlesmicroservice.models.Article;
import com.articles.articlesmicroservice.models.ArticleCategory;
import com.articles.articlesmicroservice.models.ArticleKeyword;
import com.articles.articlesmicroservice.services.ArticleService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebMvcTest(ArticleController.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ArticleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ArticleService articleService;

    private static final Article firstArticle = new Article();
    private static final Article secondArticle = new Article();
    private static final Article thirdArticle = new Article();
    private static final List<Article> articles = new ArrayList<>();

    @BeforeAll
    static void setup() {
        ArticleCategory firstCategory = new ArticleCategory();
        ArticleKeyword firstKeyword = new ArticleKeyword();
        firstArticle.setTitle("Ballon");
        firstArticle.setDescription("Ballon rouge");
        firstArticle.setPrice(9.99);
        firstCategory.setName("Jouet");
        firstKeyword.setName("balle");
        firstArticle.getCategories().add(firstCategory);
        firstArticle.getKeywords().add(firstKeyword);
        firstArticle.setSellerId(1L);

        ArticleCategory secondCategory = new ArticleCategory();
        ArticleKeyword secondKeyword = new ArticleKeyword();
        secondArticle.setTitle("Samsung Galaxy 25");
        secondArticle.setDescription("Samsung 28go RAM, 5 coeurs");
        secondArticle.setPrice(399.99);
        secondCategory.setName("Smartphone");
        secondKeyword.setName("samsung");
        secondArticle.getCategories().add(secondCategory);
        secondArticle.getKeywords().add(secondKeyword);
        secondArticle.setSellerId(1L);

        ArticleCategory thirdCategory = new ArticleCategory();
        ArticleKeyword thirdKeyword = new ArticleKeyword();
        thirdArticle.setTitle("Iphone 20 Ultra");
        thirdArticle.setDescription("Téléphone Apple couleur noir");
        thirdArticle.setPrice(1299.99);
        thirdCategory.setName("Smartphone");
        thirdKeyword.setName("iphone");
        thirdArticle.getCategories().add(thirdCategory);
        thirdArticle.getKeywords().add(thirdKeyword);
        thirdArticle.setSellerId(2L);

        articles.addAll(Arrays.asList(firstArticle, secondArticle, thirdArticle));
    }

    @Test
    public void testAddArticle() throws Exception {
        when(articleService.addArticle(any(Article.class))).thenReturn(firstArticle);
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/articles")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"title\":\"Ballon\", \"description\":\"Ballon rouge\", \"price\":9.99, \"categories\":[{\"name\":\"Jouet\"}], \"keywords\":[{\"name\":\"balle\"}], \"sellerId\":1}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title").value("Ballon"))
                .andExpect(jsonPath("$.description").value("Ballon rouge"))
                .andExpect(jsonPath("$.price").value(9.99))
                .andExpect(jsonPath("$.categories[0].name").value("Jouet"))
                .andExpect(jsonPath("$.keywords[0].name").value("balle"))
                .andExpect(jsonPath("$.sellerId").value(1L));
    }

    @Test
    public void testGetArticlesByCategory() throws Exception {
        List<Article> filteredArticles = articles
                .stream()
                .filter(article -> article
                        .getCategories()
                        .stream()
                        .anyMatch(category -> category.getName().equals("Smartphone")))
                .collect(Collectors.toList());

        when(articleService.getArticlesByCategory("Smartphone")).thenReturn(filteredArticles);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/articles/category/Smartphone"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title").value("Samsung Galaxy 25"))
                .andExpect(jsonPath("$[1].title").value("Iphone 20 Ultra"));
    }

    @Test
    public void testGetArticlesByKeyword() throws Exception {
        List<Article> filteredArticles = articles
                .stream()
                .filter(article -> article
                        .getKeywords()
                        .stream()
                        .anyMatch(keyword -> keyword.getName().equals("samsung")))
                .collect(Collectors.toList());

        when(articleService.getArticlesByKeyword("samsung")).thenReturn(filteredArticles);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/articles/keyword/samsung"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title").value("Samsung Galaxy 25"));
    }

    @Test
    public void testGetArticles() throws Exception {
        when(articleService.getArticles()).thenReturn(articles);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/articles/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3));
    }

    @Test
    public void testGetSellerArticles() throws Exception {
        List<Article> filteredArticles = articles
                .stream()
                .filter(article -> article.getSellerId() == 2L)
                .collect(Collectors.toList());

        when(articleService.getSellerArticles(2L)).thenReturn(filteredArticles);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/articles/seller/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title").value("Iphone 20 Ultra"));
    }

}
