package com.articles.articlesmicroservice.mappers;

import com.articles.articlesmicroservice.models.ArticleCategory;
import com.articles.articlesmicroservice.models.ArticleKeyword;
import com.articles.articlesmicroservice.models.dtos.ArticleCategoryDTO;
import com.articles.articlesmicroservice.models.dtos.ArticleKeywordDTO;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MapperTest {

    @Test
    public void testArticleCategoryToDTO() {
        ArticleCategory articleCategory = new ArticleCategory();
        articleCategory.setName("Category");

        ArticleCategoryDTO articleCategoryDTO = ArticleCategoryMapper.toDTO(articleCategory);

        assertEquals(articleCategory.getName(), articleCategoryDTO.getName());
    }

    @Test
    public void testArticleCategoryToEntity() {
        ArticleCategoryDTO articleCategoryDTO = new ArticleCategoryDTO();
        articleCategoryDTO.setName("Category");

        ArticleCategory articleCategory = ArticleCategoryMapper.toEntity(articleCategoryDTO);

        assertEquals(articleCategoryDTO.getName(), articleCategory.getName());
    }

    @Test
    public void testArticleCategoryListToDTOList() {
        ArticleCategory articleCategory1 = new ArticleCategory();
        articleCategory1.setName("Category1");

        ArticleCategory articleCategory2 = new ArticleCategory();
        articleCategory2.setName("Category2");

        List<ArticleCategory> articleCategories = Arrays.asList(articleCategory1, articleCategory2);

        List<ArticleCategoryDTO> articleCategoryDTOs = ArticleCategoryMapper.toDTOList(articleCategories);

        assertEquals(articleCategories.size(), articleCategoryDTOs.size());
        assertEquals(articleCategory1.getName(), articleCategoryDTOs.get(0).getName());
        assertEquals(articleCategory2.getName(), articleCategoryDTOs.get(1).getName());
    }

    @Test
    public void testArticleCategoryDTOListToEntityList() {
        ArticleCategoryDTO articleCategoryDTO1 = new ArticleCategoryDTO();
        articleCategoryDTO1.setName("Category1");

        ArticleCategoryDTO articleCategoryDTO2 = new ArticleCategoryDTO();
        articleCategoryDTO2.setName("Category2");

        List<ArticleCategoryDTO> articleCategoryDTOs = Arrays.asList(articleCategoryDTO1, articleCategoryDTO2);

        List<ArticleCategory> articleCategories = ArticleCategoryMapper.toEntityList(articleCategoryDTOs);

        assertEquals(articleCategoryDTOs.size(), articleCategories.size());
        assertEquals(articleCategoryDTO1.getName(), articleCategories.get(0).getName());
        assertEquals(articleCategoryDTO2.getName(), articleCategories.get(1).getName());
    }

    @Test
    public void testArticleKeywordToDTO() {
        ArticleKeyword articleKeyword = new ArticleKeyword();
        articleKeyword.setName("Keyword");

        ArticleKeywordDTO articleKeywordDTO = ArticleKeywordMapper.toDTO(articleKeyword);

        assertEquals(articleKeyword.getName(), articleKeywordDTO.getName());
    }

    @Test
    public void testArticleKeywordToEntity() {
        ArticleKeywordDTO articleKeywordDTO = new ArticleKeywordDTO();
        articleKeywordDTO.setName("Keyword");

        ArticleKeyword articleKeyword = ArticleKeywordMapper.toEntity(articleKeywordDTO);

        assertEquals(articleKeywordDTO.getName(), articleKeyword.getName());
    }

    @Test
    public void testArticleKeywordListToDTOList() {
        ArticleKeyword articleKeyword1 = new ArticleKeyword();
        articleKeyword1.setName("Keyword1");

        ArticleKeyword articleKeyword2 = new ArticleKeyword();
        articleKeyword2.setName("Keyword2");

        List<ArticleKeyword> articleKeywords = Arrays.asList(articleKeyword1, articleKeyword2);

        List<ArticleKeywordDTO> articleKeywordDTOs = ArticleKeywordMapper.toDTOList(articleKeywords);

        assertEquals(articleKeywords.size(), articleKeywordDTOs.size());
        assertEquals(articleKeyword1.getName(), articleKeywordDTOs.get(0).getName());
        assertEquals(articleKeyword2.getName(), articleKeywordDTOs.get(1).getName());
    }

    @Test
    public void testArticleKeywordDTOListToEntityList() {
        ArticleKeywordDTO articleKeywordDTO1 = new ArticleKeywordDTO();
        articleKeywordDTO1.setName("Keyword1");

        ArticleKeywordDTO articleKeywordDTO2 = new ArticleKeywordDTO();
        articleKeywordDTO2.setName("Keyword2");

        List<ArticleKeywordDTO> articleKeywordDTOs = Arrays.asList(articleKeywordDTO1, articleKeywordDTO2);

        List<ArticleKeyword> articleKeywords = ArticleKeywordMapper.toEntityList(articleKeywordDTOs);

        assertEquals(articleKeywordDTOs.size(), articleKeywords.size());
        assertEquals(articleKeywordDTO1.getName(), articleKeywords.get(0).getName());
        assertEquals(articleKeywordDTO2.getName(), articleKeywords.get(1).getName());
    }
}
